package Metiers;

import java.util.List;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import obj.Menu;
import obj.Produit;

@Stateful
public class GestionPromotion implements GestionPromotionLocal {

    @PersistenceContext(unitName = "projetRestaurant-ejbPU")
    private EntityManager em;

    @Override
    public List<Produit> getProduitByPromo(Long id) {
        String req = "select p.produits from Promotion p "
                + " where p.id = :paramPromo";
        Query qr = em.createQuery(req);
        qr.setParameter("paramPromo", id);
        List<Produit> produits = qr.getResultList();
        System.out.println("produit by promo DAMIEN"+produits);
        return produits;
    }

    @Override
    public List<Menu> getMenusByPromo(String intitule) {
        String req = "select p.menus from Promotion p "
                + "where p.intitule = :paramPromo";
        Query qr = em.createQuery(req);
        qr.setParameter("paramPromo", intitule);
        List<Menu> menus = qr.getResultList();
        return menus;
    }
//    a terminer
//    public float getPrixPromotion(String intitule, String nom){
//        String req = "select p.prix from Produit p"
//                + "where p.nom.intitule = :paramPrix";
//        Query qr = em.createQuery(req);
//        qr.setParameter("paramPrix", intitule);
//        float prix = qr.getMaxResults();
//        return prix;
//                    
//    }

}
