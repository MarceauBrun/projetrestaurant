
package Metiers;

import java.util.List;
import javax.ejb.Local;
import obj.Menu;
import obj.Produit;


@Local
public interface GestionPromotionLocal {
    public List<Produit> getProduitByPromo(Long id);
     public List<Menu> getMenusByPromo(String intitule);
     
    
    
    
    
    
}
