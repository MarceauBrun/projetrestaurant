/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Metiers;

import java.util.List;
import javax.ejb.Local;
import obj.Menu;
import obj.Produit;
import obj.SousCategorie;


@Local
public interface GestionCarteLocal {
   
    public List<SousCategorie> getAllSousCategories();   
    public List<Produit> getProduitsBySousCategorie(String intituleSC);  
    public List<Produit> getProduitById(int id);
    public List<Produit> getProduitByMenu (String nomMenu); 
    public List<Menu> getAllMenus();   
}