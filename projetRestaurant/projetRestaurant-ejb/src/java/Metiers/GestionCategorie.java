

package Metiers;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import obj.Categorie;


@Stateless
public class GestionCategorie implements GestionCategorieLocal {
    @PersistenceContext(unitName = "projetRestaurant-ejbPU")
    private EntityManager em;
    
    
    
    @Override
    public List<Categorie> findAllCategories(){
        String req = "select c from Categorie c ";
        Query qr = em.createQuery(req);
        List<Categorie> categories = qr.getResultList();
        return categories;
    }
    
    
    @Override
    public List<Categorie> findCategoriesByIntitule(String catIntitule){
        String req = "select c from Categorie c "
                + "where c.intitule = :paramCat";
        Query qr = em.createQuery(req);
        qr.setParameter("paramCat", catIntitule);
        List<Categorie> categories = qr.getResultList();
        return categories;
    }
    

    
}
