
package Metiers;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import obj.SousCategorie;


@Stateless
public class GestionSousCat implements GestionSousCatLocal {
    @PersistenceContext(unitName = "projetRestaurant-ejbPU")
    private EntityManager em;

    @Override
    public List<SousCategorie> findAllSousCat(){
        String req = "select s from SousCategorie s ";
        Query qr = em.createQuery(req);
        List<SousCategorie> sousCat = qr.getResultList();
        return sousCat;
    }
    
    
    @Override
    public List<SousCategorie> findSousCatByCat(Long CatId){
        String req = "select s from SousCategorie s "
                + "where s.categorie.id = :paramCat";
        Query qr = em.createQuery(req);
        qr.setParameter("paramCat", CatId);
        List<SousCategorie> sousCat = qr.getResultList();
        return sousCat;
    }

    
}
