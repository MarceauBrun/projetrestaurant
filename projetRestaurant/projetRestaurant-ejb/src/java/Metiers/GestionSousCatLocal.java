/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Metiers;

import java.util.List;
import javax.ejb.Local;
import obj.SousCategorie;

/**
 *
 * @author cdi201
 */
@Local
public interface GestionSousCatLocal {

    public List<SousCategorie> findAllSousCat();

    public List<SousCategorie> findSousCatByCat(Long CatId);
    
}
