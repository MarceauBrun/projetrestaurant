package Metiers;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import obj.Menu;
import obj.Produit;
import obj.SousCategorie;

@Stateless
public class GestionCarte implements GestionCarteLocal {
    @PersistenceContext(unitName = "projetRestaurant-ejbPU")
    private EntityManager em;

 @Override
  public List<SousCategorie> getAllSousCategories(){
     Query qr =em.createQuery("SELECT sc FROM SousCategorie sc");
      List<SousCategorie>sousCategories=qr.getResultList();
     
      return sousCategories;
  }
   
 @Override
  public List<Produit> getProduitsBySousCategorie(String intituleSC){
     Query qr = em.createQuery("SELECT s.produits FROM SousCategorie s WHERE s.intitule= :parIntituleSC"); 
     qr.setParameter("parIntituleSC", intituleSC);
     List<Produit>produits=qr.getResultList();
     return produits;
  }
  
 @Override
  public List<Produit> getProduitById(int id){
      Query qr = em.createQuery("SELECT p FROM Produit p WHERE Produit.id = :parIdProduit");
      qr.setParameter("parIdProduit", id);
      List<Produit>produits=qr.getResultList();
      return produits;    
  }
  
 @Override
  public List<Produit> getProduitByMenu (String nomMenu){
      Query qr=em.createQuery("SELECT m.produits FROM Menu m WHERE m.nom= :parNom");
      qr.setParameter("parNom", nomMenu);
      List<Produit>produits=qr.getResultList();
      return produits;
  }
  
  @Override
  public List<Menu> getAllMenus(){
     Query qr =em.createQuery("SELECT m FROM Menu m");
      List<Menu>menus=qr.getResultList();
       System.out.println("MENUSSSSSSS"+menus);
      return menus;
  }
}
