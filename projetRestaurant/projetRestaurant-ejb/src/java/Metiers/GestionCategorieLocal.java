
package Metiers;

import java.util.List;
import javax.ejb.Local;
import obj.Categorie;


@Local
public interface GestionCategorieLocal {
    
     public List<Categorie> findAllCategories();

    public List<Categorie> findCategoriesByIntitule(String catIntitule);
    
    
    
}
