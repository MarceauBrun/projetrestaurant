package essais;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import obj.Categorie;
import obj.Promotion;
import obj.SousCategorie;
import obj.Commande;
import obj.ConnexionInterface;
import obj.Cuisine;
import obj.Employe;
import obj.LigneCommande;
import obj.Menu;
import obj.Place;
import obj.Produit;
import obj.TableClient;
import obj.Tva;

@Stateless
@LocalBean
public class initObjets {

    @PersistenceContext(unitName = "projetRestaurant-ejbPU")
    private EntityManager em;

    public void initObjetsAlexMax() {
        System.out.println("\ndébut.initObjetsAlex&Max");
//PRODUITS

        /// Entrées
        Produit pro03 = new Produit("Sushi du chef", 22.10f, "au poisson frais", "slurp");
        Produit pro05 = new Produit ("Tartine gastronomique de chèvre", 6.50f, "tartine de chèvre fondu avec du miel et des noix","tartine chèvre");
        Produit pro06 = new Produit ("Tarte aux poireaux", 5.60f, "tarte BIO aux poireaux fondant", "tarte toireaux");
        Produit pro07 = new Produit ("Soupe de légumes traditionnelles", 4.50f,"Soupe de légumes frais fait maison", "soupe légumes");
        Produit pro08 = new Produit ("Tartine de marquereaux sauvage", 6.50f, "maquereux marines très frais déposé délicatement sur de fine tranches de tartines", "filet marquereaux");
        Produit pro09 = new Produit ("Capaccio de saumon fumé à la roquette", 7.50f, "fine tranche de saumon fumé sélectionné par notre chef","capaccio saumon");
        Produit pro10 = new Produit ("Salade Niçoise BIO", 6.60f,"salade niçoise composé uniquement de produits BIO et sain","Salade Niçoise");
        Produit pro11 = new Produit ("Terrine de sanglier aux champignons", 5.10f, "Terrine de sanglier Perigord avec des champignons de Paris","terrine sanglier");
        Produit pro12 = new Produit ("Assiette de Foie gras", 8.50f, "Assortiment de 3 foie gras cuisinés différements","foie gras");
        Produit pro13 = new Produit ("Omelette BIO aux champignons de Paris", 5.5f, "Omelette BIO du chef cuisiné avec des champignons BIo de Paris","omelette champignon");
        Produit pro14 = new Produit ("Quiches lorraines de la région", 6.80f,"mini quiches lorraines préparés selon une tradition ancestrale", "quiches lorraines");
        Produit pro15 = new Produit ("Bouchées à la Reine", 7.50f, "substile bouchées à la Reine de volaille BIO enrobées d'une pâte feuilleté","bouchées reine");
        
        //entrées menu
        Produit pro105 = new Produit ("Tartine gastronomique de chèvre", 6.50f, "tartine de chèvre fondu avec du miel et des noix","tartine chèvre");
        Produit pro106 = new Produit ("Tarte aux poireaux", 5.60f, "tarte BIO aux poireaux fondant", "tarte toireaux");
        Produit pro107 = new Produit ("Soupe de légumes traditionnelles", 4.50f,"Soupe de légumes frais fait maison", "soupe légumes");
       
        ///Plats
        Produit pro01 = new Produit("Entrecote", 15.60f, "belle entrecote avec ses frites", "miam");
        Produit pro02 = new Produit("Burger gastronomique", 12.40f, "servi avec du bacon bio", "cool");
        Produit pro16 = new Produit ("Ravioles de homard aux truffes noires", 21.90f, "ravioles de homard bleu de Bretagne à la sauce aux truffes noires","ravioles homard");
        Produit pro17 = new Produit ("Noix de Saint-Jacques poêlées au safran", 22.80f,"savoureux noix de Saint-Jacques associé à la saveur iodée du safran","noix saint-jacques");
        Produit pro18 = new Produit ("Homard et spaghetti au truffe", 24.90f,"Demi Homard de luxe de calibre 800gr baigné d'une sauce onctuese à la truffre ","homard spaghetti");
        Produit pro19 = new Produit ("Dindonneau braisé au champagne", 26.90f,"dindonneau fermier de belle qualité cuisiné dans du champagne Moet et Chandon", "dindonneau champagne");
        Produit pro20 = new Produit ("Supreme de Volaille, rissoto aux truffes et au foie gras", 27.20f, "Supreme de volaille fermier accompagné d'un rissoto aux truffes et au foie gras", "supreme volaile");
        Produit pro21 = new Produit ("Filet de boeuf à la truffe", 28.90f, "Boeuf français de qualité relevé par sa sauce aux truffes", "filet boeuf");
        Produit pro22 = new Produit ("Entrecote de boeuf de Kobe", 54.90f, "Entrecote de boeuf japonais de Kobe aux épices japonais", "entrecote boeuf");
        Produit pro23 = new Produit ("Turbot en papillotes au champagne", 32.90f, "filets de turbot tendres et savoureux cuits en papillotes avec une julienne de légumes","turbot papillotes");
        Produit pro24 = new Produit ("Poularde au champagne", 33.80f, "Volaille de Bresse fermier préparée dans du champagne Mumm Cordon Rouge", "poularde champagne");
        Produit pro25 = new Produit ("Nids de pates aux truffes et homard", 36.80f, "homard bleu de Bretagne enveloppé de pate Agnesi à la sauce au truffe", "nids pates");
        //plats menu
        Produit pro119 = new Produit ("Dindonneau braisé au champagne", 26.90f,"dindonneau fermier de belle qualité cuisiné dans du champagne Moet et Chandon", "dindonneau champagne");
        Produit pro120 = new Produit ("Supreme de Volaille, rissoto aux truffes et au foie gras", 27.20f, "Supreme de volaille fermier accompagné d'un rissoto aux truffes et au foie gras", "supreme volaile");
        Produit pro121 = new Produit ("Filet de boeuf à la truffe", 28.90f, "Boeuf français de qualité relevé par sa sauce aux truffes", "filet boeuf");
        
        ///Desserts
        Produit pro04 = new Produit("Glace du chef", 7.50f, "veritable glace maison", "y'a bon");
        Produit pro26 = new Produit ("Tiramisu aux spéculos",8.90f,"","tiramisu speculos");
        Produit pro27 = new Produit ("Bavarois framboises passion",7.9f,"","bavarois framboises");
        Produit pro28 = new Produit ("Bavarois pistache au caramel",7.9f,"","bavarois pistache ");
        Produit pro29 = new Produit ("Brioche Polonaise", 6.9f,"","brioche polonais");
        Produit pro30 = new Produit ("Brownies chocolat pistache",7.4f, "","brownies chocolat");
        Produit pro31 = new Produit ("Charlotte fraise myrtilles",7.9f,"","charlotte marrons");
        Produit pro32 = new Produit ("Charlotte à la framboise",8.7f,"","charlotte marrons");
        Produit pro33 = new Produit ("Cheesecake passion mangue", 9.8f, "","cheesecake passion");
        Produit pro34 = new Produit ("Creme brulé passion, chantilly Jivara", 8.5f, "", "creme brulé");
        Produit pro35 = new Produit ("Creme caramel", 8.0f, "", "creme caramel");
        //desserts menu
        Produit pro129 = new Produit ("Brioche Polonaise", 6.9f,"","brioche polonais");
        Produit pro130 = new Produit ("Brownies chocolat pistache",7.4f, "","brownies chocolat");
        Produit pro131 = new Produit ("Charlotte fraise myrtilles",7.9f,"","charlotte marrons");
        
        ///Softs       
        Produit pro36 = new Produit ("Coca cola",4.5f,"","Coca");
        Produit pro37 = new Produit ("Limonade lorina", 4.5f,"","limonade");
        Produit pro38 = new Produit ("Jus d'orange pressé",5.5f,"","jus d'orange");
        Produit pro39 = new Produit ("Jus de coco frais", 5.5f,"","jus de coco");
        Produit pro40 = new Produit ("Jus d'ananas",5.5f,"","jus d'ananas");
        //softs menu
        Produit pro136 = new Produit ("Coca cola",4.5f,"","Coca");
        Produit pro137 = new Produit ("Limonade lorina", 4.5f,"","limonade");
       
        ///Vins       
        Produit pro41= new Produit ("Vin rouge Saint Emilion Grand Cru 2012 ",56.9f,"Château Grand Pontet  Saint Emilion Grand Cru 2012 de 75 cl","Saint Emilion");
        Produit pro42= new Produit ("Vin rouge Jean Bouchard Nuits St Georges 1er Cru 2011",32.5f,"1er Cru Grand Vin de Bourgogne 2011 75 cl","Jean Bouchard");
        Produit pro43= new Produit ("Vin Blanc Jurançon 2014",28.5f,"AOP vin blanc moelleux 75 cl","Jurançon");
        Produit pro44= new Produit ("Vin Blanc Domaine Vacheron Sancerre 2016",33.5f,"Domaine Vacheron Sancerre Blanc de 75 cl","Vacheron Sancerre");
        Produit pro45= new Produit ("Vin rosé Château Sainte Marguerite Rosé Cru Classé 2016",28.5f,"Château Sainte Marguerite Rosé Côtes De Provence de 75 cl","Sainte Marguerite");
        
        ///Bieres    
        Produit pro46= new Produit ("Blanche du Mont Blanc",6.9f,"biere blonde 33 cl","mont Blanc");
        Produit pro47= new Produit ("Edelweiss",5.9f,"biere blonde 33 cl","edelweiss");
        Produit pro48= new Produit ("La Levrette", 6.9f,"biere blonde 33 cl","la Levrette");
        Produit pro49= new Produit ("La Levrette cherry", 6.9f,"biere rubis 33 cl","la Levrette cherry");
        Produit pro50= new Produit ("Bière du démon",6.9f,"biere blonde 33 cl","demon");
        
        ///Les shots       
        Produit pro51= new Produit ("Tequila PAF",4.0f,"","Tequila");
        Produit pro52= new Produit ("Le Jagerbomb",4.0f,"","Jagerbomb");
        Produit pro53= new Produit ("Le B52",4.0f,"","B52");
        Produit pro54= new Produit ("La Grappa",4.0f,"","Grappa");
        Produit pro55= new Produit ("Kamikaze",4.0f,"","Kamikaze");
                
        ///Boissons chaudes                     
        Produit pro56= new Produit ("Cafe creme",4.9f,"","Cafe creme");
        Produit pro57= new Produit ("Expresso",3.9f,"","Expresso");
        Produit pro58= new Produit ("Thé earl grey",3.5f,"","earl grey");
        Produit pro59= new Produit ("lait chaud",3.2f,"","lait chaud");
        Produit pro60= new Produit ("chocolat viennois",4.9f,"","chocolat viennois");
        //Boissons chaudes menu
        Produit pro156= new Produit ("Cafe creme",4.9f,"","Cafe creme");
        Produit pro157= new Produit ("Expresso",3.9f,"","Expresso");
//instanciation des menus
        Menu m01 = new Menu("Menu 15", "menu du jour entrée,plat", 15, ":-)");
        Menu m02 = new Menu("Menu 18", "menu entrée,plat ou plat/dessert", 18, ":-)");
        Menu m03 = new Menu("Menu 22", "entrée, plat, dessert, boisson soft", 20, ":-)");
        Menu m04 = new Menu("Menu 25", "menu gastronomique: entrée, plat, dessert, boisson soft, boisson chaude", 25, ":-)");
        Collection<Menu> ms01 = new ArrayList<>();      
        ms01.add(m01);
        ms01.add(m02);
        ms01.add(m03);
        ms01.add(m04);
                
//instanciation des promos
        Categorie c01 = new Categorie("Les Mets");
        Categorie c02 = new Categorie("Boissons");
        Categorie c03 = new Categorie("Boissons alcoolisées");

        SousCategorie sc01 = new SousCategorie("Plats","les plats","src=\"images/kabob.jpg\" width=\"250\" height=\"220\"");
        SousCategorie sc02 = new SousCategorie("Entrees");
        SousCategorie sc03 = new SousCategorie("Desserts");
        SousCategorie sc04 = new SousCategorie("Softs");
        SousCategorie sc05 = new SousCategorie("Vins");
        SousCategorie sc06 = new SousCategorie("Bieres");
        SousCategorie sc07 = new SousCategorie("Les shots");
        SousCategorie sc08 = new SousCategorie("Boissons chaudes");
        SousCategorie sc09 = new SousCategorie("Les salades");
        SousCategorie sc10 = new SousCategorie("Fromages");
        SousCategorie sc11 = new SousCategorie("entreeMenu");
        SousCategorie sc12 = new SousCategorie("platMenu");
        SousCategorie sc13 = new SousCategorie("dessertMenu");
        SousCategorie sc14 = new SousCategorie("softsMenu");
        SousCategorie sc15 = new SousCategorie("boissonsChaudesMenu");
        
        Collection<SousCategorie> scs01 = new ArrayList<>();
        Collection<SousCategorie> scs02 = new ArrayList<>();
        Collection<SousCategorie> scs03 = new ArrayList<>();
        Collection<SousCategorie> scs04 = new ArrayList<>();

//creation des objets date
        Date date01 = new GregorianCalendar(2017, 06, 22).getTime();
        Date date02 = new GregorianCalendar(2017, 06, 23).getTime();
        Date date03 = new GregorianCalendar(2017, 07, 05).getTime();
        Date date04 = new GregorianCalendar(2017, 07, 06).getTime();
//instanciation des promos
        Promotion p01 = new Promotion("Happy Monday", date01, date02, 10);
        Promotion p02 = new Promotion("Happy Friday", date03, date04, 15);             
   //les assos     
        
        //les produits dans les menus
        //entrées menu
        pro105.setSousCategorie(sc11);
        pro106.setSousCategorie(sc11);
        pro107.setSousCategorie(sc11);
        em.persist(pro105);
        em.persist(pro106);
        em.persist(pro107);
        //plats menu
        pro119.setSousCategorie(sc12);
        pro120.setSousCategorie(sc12);
        pro121.setSousCategorie(sc12);
        em.persist(pro119);
        em.persist(pro120);
        em.persist(pro121);
        //dessert menu
        pro129.setSousCategorie(sc13);
        pro130.setSousCategorie(sc13);
        pro131.setSousCategorie(sc13);
        em.persist(pro129);
        em.persist(pro130);
        em.persist(pro131);
        //softs menu
        pro136.setSousCategorie(sc14);
        pro137.setSousCategorie(sc14);
        em.persist(pro136);
        em.persist(pro137);
        //boissons chaudes menu
        pro156.setSousCategorie(sc15);
        pro157.setSousCategorie(sc15);
        em.persist(pro156);
        em.persist(pro157);
        
        sc11.getMenus().add(m01);
        sc12.getMenus().add(m01);
        sc11.getMenus().add(m02);
        sc12.getMenus().add(m02);
        sc13.getMenus().add(m02);
        sc11.getMenus().add(m03);
        sc12.getMenus().add(m03);
        sc13.getMenus().add(m03);
        sc14.getMenus().add(m03);
        sc11.getMenus().add(m04);
        sc12.getMenus().add(m04);
        sc13.getMenus().add(m04);
        sc14.getMenus().add(m04);
        sc15.getMenus().add(m04);

      //  m01.setSousCategories(scs01);
        sc01.setCategorie(c01);
        sc02.setCategorie(c01);
        sc03.setCategorie(c01);
        sc09.setCategorie(c01);
        sc10.setCategorie(c01);
        sc04.setCategorie(c02);
        sc08.setCategorie(c02);
        sc05.setCategorie(c03);
        sc06.setCategorie(c03);
        sc07.setCategorie(c03);

       pro01.setPromotion(p01);
       pro02.setPromotion(p01);
       pro03.setPromotion(p02);
       pro04.setPromotion(p02);
       
        sc01.setMenus(ms01);

        //PERSISTANCE//
        em.persist(sc01);
        em.persist(sc02);
        em.persist(sc03);
        em.persist(sc04);
        em.persist(sc05);
        em.persist(sc06);
        em.persist(sc07);
        em.persist(sc08);
        em.persist(sc09);
        em.persist(sc10);

        em.persist(p01);
        em.persist(p02);
        
        
        
        //persists max 
         pro03.setSousCategorie(sc02);
        pro05.setSousCategorie(sc02);
        pro06.setSousCategorie(sc02);
        pro07.setSousCategorie(sc02);
        pro08.setSousCategorie(sc02);
        pro09.setSousCategorie(sc02);
        pro10.setSousCategorie(sc02);
        pro11.setSousCategorie(sc02);
        pro12.setSousCategorie(sc02);
        pro13.setSousCategorie(sc02);
        pro14.setSousCategorie(sc02);
        pro15.setSousCategorie(sc02);
        em.persist(pro03);
        em.persist(pro05);
        em.persist(pro06);
        em.persist(pro07);
        em.persist(pro08);
        em.persist(pro09);
        em.persist(pro10);
        em.persist(pro11);
        em.persist(pro12);
        em.persist(pro13);
        em.persist(pro14);
        em.persist(pro15);
       
        
        em.persist(pro01);
        em.persist(pro02);
        em.persist(pro16);
        em.persist(pro17);
        em.persist(pro18);
        em.persist(pro19);
        em.persist(pro20);
        em.persist(pro21);
        em.persist(pro22);
        em.persist(pro23);
        em.persist(pro24);
        em.persist(pro25);
        em.persist(pro25);
       pro01.setSousCategorie(sc01);
       pro02.setSousCategorie(sc01);
       pro16.setSousCategorie(sc01);
        pro17.setSousCategorie(sc01);
        pro18.setSousCategorie(sc01);
        pro19.setSousCategorie(sc01);
        pro20.setSousCategorie(sc01);
        pro21.setSousCategorie(sc01);
        pro22.setSousCategorie(sc01);
        pro23.setSousCategorie(sc01);
        pro24.setSousCategorie(sc01);
        pro25.setSousCategorie(sc01);
      
        em.persist(pro26);
        em.persist(pro27);
        em.persist(pro28);
        em.persist(pro29);
        em.persist(pro30);
        em.persist(pro31);
        em.persist(pro32);
        em.persist(pro33);
        em.persist(pro34);
        em.persist(pro35);
        pro04.setSousCategorie(sc03);
        pro26.setSousCategorie(sc03);
        pro27.setSousCategorie(sc03);
        pro28.setSousCategorie(sc03);
        pro29.setSousCategorie(sc03);
        pro30.setSousCategorie(sc03);
        pro31.setSousCategorie(sc03);
        pro32.setSousCategorie(sc03);
        pro33.setSousCategorie(sc03);
        pro34.setSousCategorie(sc03);
        pro35.setSousCategorie(sc03);
        
         em.persist(pro36);
        em.persist(pro37);
        em.persist(pro38);
        em.persist(pro39);
        em.persist(pro40);
        pro36.setSousCategorie(sc04);
        pro37.setSousCategorie(sc04);
        pro38.setSousCategorie(sc04);
        pro39.setSousCategorie(sc04);
        pro40.setSousCategorie(sc04);
        
        em.persist(pro41);
        em.persist(pro42);
        em.persist(pro43);
        em.persist(pro44);
        em.persist(pro45);
        pro41.setSousCategorie(sc05);
        pro42.setSousCategorie(sc05);
        pro43.setSousCategorie(sc05);
        pro44.setSousCategorie(sc05);
        pro45.setSousCategorie(sc05);
        
        em.persist(pro46);
        em.persist(pro47);
        em.persist(pro48);
        em.persist(pro49);
        em.persist(pro50);
        pro46.setSousCategorie(sc06);
        pro47.setSousCategorie(sc06);
        pro48.setSousCategorie(sc06);
        pro49.setSousCategorie(sc06);
        pro50.setSousCategorie(sc06);
        
        em.persist(pro51);
        em.persist(pro52);
        em.persist(pro53);
        em.persist(pro54);
        em.persist(pro55);
        pro51.setSousCategorie(sc07);
        pro52.setSousCategorie(sc07);
        pro53.setSousCategorie(sc07);
        pro54.setSousCategorie(sc07);
        pro55.setSousCategorie(sc07);
        
        em.persist(pro56);
        em.persist(pro57);
        em.persist(pro58);
        em.persist(pro59);
        em.persist(pro60);
        pro56.setSousCategorie(sc08);
        pro57.setSousCategorie(sc08);
        pro58.setSousCategorie(sc08);
        pro59.setSousCategorie(sc08);
        pro60.setSousCategorie(sc08);
        
         //TVA
        Tva tva01 = new Tva ("produit à consommation différée", 0.055f);
        Tva tva02 = new Tva ("produit à consommation immédiate", 0.10f);
        Tva tva03 = new Tva ("boisson alcolisé",0.20f);

        em.persist(tva01);
        em.persist(tva02);
        em.persist(tva03);
        
        System.out.println("fin.initObjetsALEX&MAX");
    }

    public void initObjetsMarceau() {
        System.out.println("\ndebut.initObjetsMarceau");
        
        Place p01 = new Place(1);
        Place p02 = new Place(2);
        Place p03 = new Place(3);
        Place p04 = new Place(4);
        Place p05 = new Place(5);
        Place p06 = new Place(6);
        Place p07 = new Place(7);
        Place p08 = new Place(8);
        
        Collection<Place> places = new ArrayList<>();
        Collection<Place> places2 = new ArrayList<>();
//        places.add(p01);
//        places.add(p02);
//        places.add(p03);
//        places.add(p04);
//        places.add(p05);
//        places.add(p06);
//
//        places2.add(p01);
//        places2.add(p02);
//        places2.add(p03);
//        places2.add(p04);
//        places2.add(p05);
//        places2.add(p06);
//        places2.add(p07);
//        places2.add(p08); 
        TableClient tc01 = new TableClient(1,6);
        p01.setTable(tc01);
        p02.setTable(tc01);
        p03.setTable(tc01);
        p04.setTable(tc01);
        p05.setTable(tc01);
        p06.setTable(tc01);
      //  TableClient tc02 = new TableClient(2,6);
      //  tc01.setPlaces(places);
      //  tc02.setPlaces(places2);

        System.out.println("persist.initObjetsMarceau");

//        p02.getTable().add(tc01);
//        p03.getTable().add(tc01);
//        p04.getTable().add(tc01);
//        p05.getTable().add(tc01);
//        p06.getTable().add(tc01);
//        tc01.getPlaces().add(p01);
//        tc01.getPlaces().add(p03);
//        tc01.getPlaces().add(p04);
//        tc01.getPlaces().add(p05);
//        tc01.getPlaces().add(p06);
//        em.persist(tc01);
        
        em.persist(p01);
        em.persist(p02);
        em.persist(p03);
        em.persist(p04);
        em.persist(p05);
        em.persist(p06);
        em.persist(p07);
        em.persist(p08);
/////////////////////////////////////////////////////////////////////////////////////////

        Date d01 = new GregorianCalendar(2017, 05, 25, 12, 10).getTime();
        Date d02 = new GregorianCalendar(2017, 05, 25, 12, 45).getTime();
        Date d03 = new GregorianCalendar(2017, 05, 25, 11, 55).getTime();
        Date d04 = new GregorianCalendar(2017, 05, 25, 12, 30).getTime();
       
        Commande c01 = new Commande(2, d01, d02);
        Commande c02 = new Commande(1, d03, d04);
       // Collection<Commande> commandes = new ArrayList<>();
        
        //commandes.add(c02);
        //commandes.add(c01);
//      commandes.add(c02); 
      //  tc01.setCommandes(commandes);
      //  c01.setTable(tc01);

        LigneCommande lc01 = new LigneCommande(15f, 0, 5, 1, 0, d01, null);
        LigneCommande lc02 = new LigneCommande(18.5f, 0, 5, 1, 0, d01, null);
     //   Collection<LigneCommande> lignesCommande = new ArrayList<>();

      //  lignesCommande.add(lc01);
      //  lignesCommande.add(lc02);
       // c01.setLignesCommandes(lignesCommande);
        lc01.setCommande(c01);
        lc02.setCommande(c01);

        Cuisine cu01 = new Cuisine("envoyé", "aucun soucis");
         lc01.setCuisine(cu01);
         lc02.setCuisine(cu01);
//        c01.setLignesCommandes(lignesCommande);       
//        lc01.setCuisine(cu01); 
//        lc02.setCuisine(cu01);
   //    em.persist(tc01);
//        em.persist(tc02);
    //    lignesCommande.add(lc01);
   //     lignesCommande.add(lc02);
    //    c01.setLignesCommandes(lignesCommande);
   //     lc01.setCommande(c01);
   //     lc02.setCommande(c01);
  //      c01.setLignesCommandes(lignesCommande);
 //       lc01.setCuisine(cu01);
 //       lc02.setCuisine(cu01);
     //   em.persist(cu01);
        
        em.persist(lc01);
      //  em.persist(lc02);
//        em.persist(c01);
//        em.persist(lc01);
//        em.persist(lc02);
//        em.persist(p01);
//        em.persist(p02);
//        em.persist(p03);
//        em.persist(p04);
//        em.persist(p05);
//        em.persist(p06);

        System.out.println("fin.initObjetsMarceau");
    }

    public void initObjetsDamien() {
        System.out.println("\ndebut.initObjetsDamien");
        Employe emp01 = new Employe("Pierre", "Dupont", "serveur", ";-)");
        Employe emp02 = new Employe("Alex", "Djuka", "cuisinier", ";-)");
        Employe emp03 = new Employe("Max", "Lamenace", "serveur", ";-)");
        Employe emp04 = new Employe("Marceau", "Brun", "serveur", ";-)");

        em.persist(emp01);
        em.persist(emp02);
        em.persist(emp03);
        em.persist(emp04);

        ConnexionInterface con01 = new ConnexionInterface(1001, "service", emp01);
        ConnexionInterface con02 = new ConnexionInterface(2001, "cuisine", emp02);
        ConnexionInterface con03 = new ConnexionInterface(1002, "service", emp03);
        ConnexionInterface con04 = new ConnexionInterface(1003, "service", emp04);

        em.persist(con01);
        em.persist(con02);
        em.persist(con03);
        em.persist(con04);


     //   Cuisine cui01 = new Cuisine("en préparation", "bon");        
//        Statut sta01 = new Statut("modifié",01,"")       
        System.out.println("fin.initObjetsDamien");
    }

       
       
}
