package obj;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.*;

@Entity
public class Alcool implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    private float tauxAlc;
    

    @OneToMany(mappedBy = "alcool")
    private Collection<Ingredient> ingredients;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Alcool() {
    }

    public Alcool(Long id, float tauxAlc, Collection<Ingredient> ingredients) {
        this.id = id;
        this.tauxAlc = tauxAlc;
        this.ingredients = ingredients;
    }

    public float getTauxAlc() {
        return tauxAlc;
    }

    public void setTauxAlc(float tauxAlc) {
        this.tauxAlc = tauxAlc;
    }

    public Collection<Ingredient> getIngredients() {
        return ingredients;
    }

    public void setIngredients(Collection<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }
    
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Alcool)) {
            return false;
        }
        Alcool other = (Alcool) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "obj.Alcool[ id=" + id + " ]";
    }
    
}
