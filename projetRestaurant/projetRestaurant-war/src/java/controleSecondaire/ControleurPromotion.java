
package controleSecondaire;

import Metiers.GestionPromotionLocal;
import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import obj.Produit;



public class ControleurPromotion implements Serializable,ControleurInterface{
    GestionPromotionLocal gestionPromotion = lookupGestionPromotionLocal();

    
    
    @Override
    public String executer(HttpServletRequest request, HttpServletResponse response) {
        
        List<Produit> prodPromo=gestionPromotion.getProduitByPromo(2l);
        System.out.println("list Damienovic promo produits"+prodPromo);
        request.setAttribute("prodPromo", prodPromo);
        return "/WEB-INF/Client.jsp"; 
    }

    private GestionPromotionLocal lookupGestionPromotionLocal() {
        try {
            Context c = new InitialContext();
            return (GestionPromotionLocal) c.lookup("java:global/projetRestaurant/projetRestaurant-ejb/GestionPromotion!Metiers.GestionPromotionLocal");
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }
  
  
    
    
    
    
}
