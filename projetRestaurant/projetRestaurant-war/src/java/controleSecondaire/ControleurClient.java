
package controleSecondaire;

import Metiers.GestionCarteLocal;
import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import obj.Menu;
import obj.Produit;
import obj.SousCategorie;


public class ControleurClient implements Serializable, ControleurInterface{
    GestionCarteLocal gestionCarte = lookupGestionCarteLocal();

     @Override
    public String executer(HttpServletRequest request, HttpServletResponse response) {
    
///////////////////// Afficher les menus //////////////////////
        List<Menu> menus = gestionCarte.getAllMenus();
         System.out.println("LIST ALEX MENUS<<<<<<<<<<<<<<<<<<<<<<<"+menus);
        request.setAttribute("menus", menus);
        
        List<SousCategorie> sCats = gestionCarte.getAllSousCategories();
         System.out.println("LIST ALEX SC<<<<<<<<<<<<<<<<<<<"+sCats);
        request.setAttribute("sCats", sCats);
        
        
        
        
        String intituleSC = request.getParameter("intituleSC");       
        List<Produit> produits = gestionCarte.getProduitsBySousCategorie(intituleSC);       
        System.out.println("LIST ALEX Produit by scats<<<<<<<<<<<<<<<"+produits);
        request.setAttribute("produits", produits);
        
        return "/WEB-INF/Client.jsp";  
           
    }

    private GestionCarteLocal lookupGestionCarteLocal() {
        try {
            Context c = new InitialContext();
            return (GestionCarteLocal) c.lookup("java:global/projetRestaurant/projetRestaurant-ejb/GestionCarte!Metiers.GestionCarteLocal");
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }

   





}



    
    
    
    

