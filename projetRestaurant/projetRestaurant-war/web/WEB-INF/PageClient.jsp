
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width,shrink-to-fit=no, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>PageClient</title>

        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="css/shop-homepage.css" rel="stylesheet">
        <link href="css/simple-sidebar.css" rel="stylesheet">       
        <link href="css/mycss.css" rel="stylesheet">
    </head>

    <body>
         <c:url var="url01" value="FrontControleur?section=menu-main" />
        <c:import url="${url01}" />
        <!-- Navigation -->
        <!-- Sidebar -->
        <div id="wrapper">
            <div id="sidebar-wrapper">
                <ul class="sidebar-nav">
                    <li class="sidebar-brand">
                        <a href="FrontControleur?section=InterfaceServeur">
                            Interface Serveur
                        </a>
                    </li>
                    <li>
                        <a href="FrontControleur?section=ControleurClient">Interface TEST</a>
                    </li>
                    <li>
                        <a href="#">La Carte</a>
                    </li>
                    <li>
                        <a href="#">Menus</a>
                    </li>
                    <li>
                        <a href="#">Entrées</a>
                    </li>
                    <li>
                        <a href="#">Plats</a>
                    </li>
                    <li>
                        <a href="#">Desserts</a>
                    </li>
                    <li>
                        <a href="#">Softs</a>
                    </li>
                    <li>
                        <a href="#">Alcools</a>
                    </li>

                </ul>
            </div>
            <!-- /#sidebar-wrapper -->
            <!-- Page Content -->
            <div id="page-content-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <h1>LES POTOS</h1>
                            <a href="#menu-toggle" class="btn btn-danger" id="menu-toggle">Menu</a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /#page-content-wrapper -->
        </div>
        <!-- Page Content -->
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="row carousel-holder">
                        <div class="col-lg-12">
                            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                                <ol class="carousel-indicators">
                                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                                </ol>
                                <div class="carousel-inner">
                                    <div class="item active">
                                        <img class="slide-image" src="images/pano1.jpg" width="800px" height="300px" alt="">
                                    </div>
                                    <div class="item">
                                        <img class="slide-image" src="images/pano2.jpg" alt="">
                                    </div>
                                    <div class="item">
                                        <img class="slide-image" src="images/pano3.jpg" alt="">
                                    </div>
                                </div>
                                <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                                    <span class="glyphicon glyphicon-chevron-left"></span>
                                </a>
                                <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                                    <span class="glyphicon glyphicon-chevron-right"></span>
                                </a>
                            </div>
                        </div>

                    </div>

                    <div class="row">

                        <div class="col-sm-3 col-lg-3 col-md-3">
                            <div class="thumbnail">
                                <img src="http://placehold.it/320x240" alt="">
                                <div class="caption">
                                    <h4 class="pull-right">15.00€</h4>
                                    <h4><a href="#">Menu 1</a>
                                    </h4>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                </div>

                            </div>
                        </div>

                        <div class="col-sm-3 col-lg-3 col-md-3">
                            <div class="thumbnail">
                                <img src="http://placehold.it/320x240" alt="">
                                <div class="caption">
                                    <h4 class="pull-right">18.00€</h4>
                                    <h4><a href="#">Menu 2</a>
                                    </h4>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                </div>

                            </div>
                        </div>

                        <div class="col-sm-3 col-lg-3 col-md-3">
                            <div class="thumbnail">
                                <img src="http://placehold.it/320x240" alt="">
                                <div class="caption">
                                    <h4 class="pull-right">22.00€</h4>
                                    <h4><a href="#">Menu 3</a>
                                    </h4>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                </div>

                            </div>
                        </div>

                        <div class="col-sm-3 col-lg-3 col-md-3">
                            <div class="thumbnail">
                                <img src="http://placehold.it/320x240" alt="">
                                <div class="caption">
                                    <h4 class="pull-right">26.00€</h4>
                                    <h4><a href="#">Menu 4</a>
                                    </h4>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.container -->
        <div class="container">
            <hr>
            <!-- Footer -->
            <footer>
                <div class="row">
                    <div class="col-lg-12">
                        <p>Copyright Les Potos</p>
                    </div>
                </div>
            </footer>
        </div>
        <!-- /.container -->
        <!-- jQuery -->
        <script src="js/jquery.js"></script>
        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>
        <!-- Menu Toggle Script -->
        <script>
            $("#menu-toggle").click(function (e) {
                e.preventDefault();
                $("#wrapper").toggleClass("toggled");
            });
        </script>
    </body>
</html>

