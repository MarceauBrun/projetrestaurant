
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">                     
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/mycss.css">
        <c:url var="urlHome" value="/WEB-INF/home.jsp"/>
        <title>Client Test</title>
    </head>
    <body>
        <c:url var="urlMenu" value="FrontControleur?section=menu-main" />
        <c:import url="${urlMenu}" />
        <hr/>
        <h2>Client Test</h2>     
        <div class="container-fluid">
            <div class=" col-lg-3">
                <c:if test="${not empty sCats}">
                <c:forEach var="sc" items="${sCats}">
                        <strong>    
                            <ul class=list-group">
                                <li class="dropdown-header"><a class="btn btn-primary" href="FrontControleur?section=ControleurClient&intituleSC=${sc.intitule}">${sc.intitule}</a></li>
                            </ul>
                        </strong>   
                               </c:forEach>
                        </c:if>    
                    </div>

                    <div class="well-lg col-lg-9 dl-horizontal">
                        <c:if test="${not empty produits}">
                            <c:forEach var="p" items="${produits}">   

                                <p class="text-content ">
                                <h2>${sc.intitule}</h2>
                                <span class="lead">${p.nom}</span>
                                <span><fmt:formatNumber value="${p.prix}" minFractionDigits="2" maxFractionDigits="2" minIntegerDigits="1" />€</span>   
                                <span class="small">${p.description}</span>  

                                </p>                                                                
                            </c:forEach>
                        </c:if>     
                    </div>
              
        </div>

    </body>
</html>
